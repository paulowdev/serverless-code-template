#!/bin/bash

# Init setup
npm i -g serverless
npm install -D typescript

# Serverless
# sls plugin install -n serverless-plugin-typescript
sls plugin install -n serverless-offline
sls plugin install -n serverless-prune-plugin
sls plugin install -n serverless-bundle

# NPM
npm install