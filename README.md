# Domain - Service name
___

## Overview

Description...

## Architecture

...

## Requirements

* [NodeJs](https://nodejs.org/en/download/releases/) | *v14+*
* [Serverless Framework](https://www.serverless.com/framework/docs/providers/fn/guide/installation#installing-the-serverless-framework)
* [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
* [Docker](https://docs.docker.com/engine/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)

## Running locally

#### AWS configure - Localstack

##### Steps:

***1. Create Localstack credentials***

```
$ aws configure --profile localstack
AWS Access Key ID : test
AWS Secret Access Key : test
Default region name [us-east-1]: us-east-1
Default output format [json]: json
```

***2. Prepare and running Localstack(AWS)***

```
$ npm run deploy:aws-local
```

...


***4. Serverless deploy***

...