import * as z from 'zod';

export const createUserRequestSchema = z.object({
    name: z.string(),
    email: z.string().email({ message: "Invalid email address" })
});

export type CreateUserRequest = z.infer<typeof createUserRequestSchema>;