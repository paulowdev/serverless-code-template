// import { inject, injectable } from "tsyringe";

import { UserEntity } from "../../../core/entities/UserEntity";
// import { DatabasePort } from "../ports/DatabasePort";

import { getUser } from "../adapters/DatabaseAdapter";

export async function execUseCase(id?: string): Promise<UserEntity> {
    const user = getUser(id!)
    return user;
}
