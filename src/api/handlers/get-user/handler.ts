import "reflect-metadata"
import { config } from "../../../shared/config"
import { sharedContainer } from "../../../shared/container";

import { GetSecretValueCommandInput, GetSecretValueCommandOutput } from '@aws-sdk/client-secrets-manager';
import { Handler, APIGatewayProxyEvent, Context, APIGatewayProxyResult } from "aws-lambda";

import { AWSProvider } from "../../../shared/infrastructure/providers/AWSProvider";
import { execUseCase } from "./use-cases/GetUserUseCase";

export const main: Handler = async (event: APIGatewayProxyEvent, context: Context): Promise<APIGatewayProxyResult> => {
    try {
        // console.log(process.env.SECRET_NAME)
        const awsProvider: AWSProvider = sharedContainer.resolve("IAWSProvider");
        const inSecretParams: GetSecretValueCommandInput = {
            SecretId: config.secret_name,
        };
        const outSecretValue: GetSecretValueCommandOutput = await awsProvider.getSecretValue(inSecretParams);

        const { id } = event.pathParameters!;

        const user = execUseCase(id);

        return {
            statusCode: 200,
            body: JSON.stringify({
                user
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'some error happened',
            }),
        };
    }
};