import { UserEntity } from "../../../core/entities/UserEntity";

export interface DatabasePort {
    getUser(id: string): Promise<UserEntity>
}