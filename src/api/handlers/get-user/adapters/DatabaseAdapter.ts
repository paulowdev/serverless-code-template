import "reflect-metadata";

import { DynamoDB } from "aws-sdk";
import { config } from "../../../../shared/config";
import { UserEntity } from "../../../core/entities/UserEntity";

// TODO: Move to class
const dbClient = new DynamoDB.DocumentClient({ region: 'us-east-1', endpoint: 'http://localhost:4566' });

export async function getUser(
    id: string
): Promise<UserEntity> {
    const params: DynamoDB.DocumentClient.GetItemInput = {
        TableName: config.dynamodb_table,
        Key: { id }
    };

    const { Item: item } = await dbClient.get(params).promise();

    return toUserEntity(item);
}

function toUserEntity(item?: DynamoDB.DocumentClient.AttributeMap): UserEntity {
    return UserEntity.parse({
        id: item?.id,
        name: item?.name,
        email: item?.email
    });
} 