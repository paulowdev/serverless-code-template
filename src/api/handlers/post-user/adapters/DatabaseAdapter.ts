import "reflect-metadata";

import { DynamoDB } from "aws-sdk";
import { config } from "../../../../shared/config";

// TODO: Move to class
const dbClient = new DynamoDB.DocumentClient({ region: 'us-east-1', endpoint: 'http://localhost:4566' });

export async function createUser(
    item: { [key: string]: any }
) {
    const params: DynamoDB.DocumentClient.PutItemInput = {
        TableName: config.dynamodb_table,
        Item: item
    };

    await dbClient.put(params).promise();
}