
import { APIGatewayProxyEvent, APIGatewayProxyResult, Context, Handler } from "aws-lambda";


import { CreateUserRequestSchema, User } from "./schemas/CreateUserRequest";
import { execUseCase } from "./use-cases/CreateUserUseCase";

export const main: Handler = async (event: APIGatewayProxyEvent, context: Context): Promise<APIGatewayProxyResult> => {
    try {

        const user: User = CreateUserRequestSchema.parse(JSON.parse(event.body!));

        await execUseCase(user);

        return {
            statusCode: 201,
            body: JSON.stringify({
                success: true,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'some error happened',
            }),
        };
    }
};