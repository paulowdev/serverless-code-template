import * as z from 'zod';

export const CreateUserRequestSchema = z.object({
    name: z.string(),
    email: z.string().email({ message: "Invalid email address" })
});

export type User = z.infer<typeof CreateUserRequestSchema>;