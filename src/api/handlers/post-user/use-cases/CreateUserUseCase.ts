import { v4 as uuid } from 'uuid';

// import { DatabasePort } from "../ports/DatabasePort";

import { createUser } from "../adapters/DatabaseAdapter";
import { User } from "../schemas/CreateUserRequest";

export async function execUseCase(user: User) {

    const item = {
        id: uuid(),
        name: user?.name,
        email: user?.email,
    }

    await createUser(item)
}
