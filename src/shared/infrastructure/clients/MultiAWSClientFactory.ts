import { defaultAwsParams } from '../helpers';

export class MultiAWSClientFactory {

  private awsParams: any;

  constructor(awsParams: any = defaultAwsParams()){
    this.awsParams = awsParams;
  }

  public create<T>(client: (new (params: any) => T)): T {
      try{
          return new client(this.awsParams);
      } catch(error){
          console.error(error)
          throw new Error(`Failed to instantiate AWS client. Error: ${error}`)
      }  
  }

}