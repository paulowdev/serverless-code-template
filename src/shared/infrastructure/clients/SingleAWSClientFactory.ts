import { defaultAwsParams } from '../helpers';

export class SingleAWSClientFactory {

  private static instance: SingleAWSClientFactory;
  private awsParams: any;

  private constructor(){
    this.awsParams = defaultAwsParams();
  }

  public static getInstance(): SingleAWSClientFactory {
    if (!SingleAWSClientFactory.instance) {
      SingleAWSClientFactory.instance = new SingleAWSClientFactory();
    }
    return SingleAWSClientFactory.instance;
}

  public create<T>(client: (new (params: any) => T)): T {
      try{
          return new client(this.awsParams);
      } catch(error){
          console.error(error)
          throw new Error(`Failed to instantiate AWS client: ${client.name}. Error: ${error}`)
      }  
  }

}