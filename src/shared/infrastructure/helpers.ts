
export const isLocalStage= () => process.env.STAGE === 'local';

export const defaultAwsParams = (): any => {
    const awsRegion: string = process.env.AWS_REGION! || process.env.AWS_DEFAULT_REGION!;
    const localStackEndpoint: string = process.env.LOCALSTACK_ENDPOINT! || 'http://localhost:4566';
    return isLocalStage() ? 
        { region: awsRegion || 'us-east-1', endpoint: localStackEndpoint } :
        { region: awsRegion || 'us-east-1' }
}