import { GetItemCommand, GetItemCommandOutput, PutItemCommand, PutItemCommandOutput,
UpdateItemCommand, UpdateItemCommandOutput, QueryCommand, QueryCommandOutput,
ScanCommand, ScanCommandOutput, DeleteItemCommand, DeleteItemCommandOutput} from "@aws-sdk/client-dynamodb";

export interface IDynamoDBProvider {
    getAllItems(queryCmd: QueryCommand): Promise<any[]>;
    getItem(getItemCmd: GetItemCommand): Promise<GetItemCommandOutput>;
    putItem(putItemCmd: PutItemCommand): Promise<PutItemCommandOutput>;
    updateItem(updateItemCmd: UpdateItemCommand): Promise<UpdateItemCommandOutput>;
    query(queryCmd: QueryCommand): Promise<QueryCommandOutput>;
    scan(scanCmd: ScanCommand): Promise<ScanCommandOutput>;
    deleteItem(deleteItemCmd: DeleteItemCommand): Promise<DeleteItemCommandOutput>;
}