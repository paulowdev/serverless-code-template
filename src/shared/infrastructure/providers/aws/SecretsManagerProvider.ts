import { ISecretsManagerProvider } from "./ISecretsManagerProvider";

import { SingleAWSClientFactory } from "../../clients/SingleAWSClientFactory";
import { SecretsManagerClient, GetSecretValueCommand, GetSecretValueCommandOutput } from '@aws-sdk/client-secrets-manager';

export class SecretsManagerProvider implements ISecretsManagerProvider {
    client: SecretsManagerClient;

    constructor(){
        this.client = SingleAWSClientFactory.getInstance().create(SecretsManagerClient);
    }

    async getSecretValue(getSecretValueCmd: GetSecretValueCommand): Promise<GetSecretValueCommandOutput> {
            return await this.client.send(getSecretValueCmd);
    }
}