import { GetSecretValueCommand, GetSecretValueCommandOutput } from '@aws-sdk/client-secrets-manager';

export interface ISecretsManagerProvider {
    getSecretValue(params: GetSecretValueCommand): Promise<GetSecretValueCommandOutput>;
}