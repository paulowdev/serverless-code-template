import { ISNSProvider } from './ISNSProvider'

import { SingleAWSClientFactory } from "../../clients/SingleAWSClientFactory";
import { SNSClient, PublishCommand } from "@aws-sdk/client-sns";

export class SNSProvider implements ISNSProvider {
    client: SNSClient;

    constructor(){
        this.client = SingleAWSClientFactory.getInstance().create(SNSClient);
    }

    async publish(messageBody: any, topicArn: string): Promise<void> {

        try {
            const cmdInput = {
                Message: JSON.stringify(messageBody),
                TopicArn: topicArn,
                MessageStructure: "string"
            };
            const command: PublishCommand = new PublishCommand(cmdInput);
            await this.client.send(command);
        } catch (err) {
            console.error(err);
            throw err;
        }
    }
}