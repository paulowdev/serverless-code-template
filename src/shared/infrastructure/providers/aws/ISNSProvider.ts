
export interface ISNSProvider {
    publish(messageBody: any, topicArn?: string): Promise<void>;
}