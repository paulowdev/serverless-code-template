import { IDynamoDBProvider } from "./IDynamoDBProvider";

import { SingleAWSClientFactory } from "../../clients/SingleAWSClientFactory";
import { DynamoDBClient, GetItemCommand, GetItemCommandOutput, PutItemCommand, PutItemCommandOutput,
    UpdateItemCommand, UpdateItemCommandOutput, QueryCommand, QueryCommandOutput,
    ScanCommand, ScanCommandOutput, DeleteItemCommand, DeleteItemCommandOutput} from "@aws-sdk/client-dynamodb";


export class DynamoDBProvider implements IDynamoDBProvider {
    client: DynamoDBClient;

    constructor(){
        this.client = SingleAWSClientFactory.getInstance().create(DynamoDBClient);
    }

    async getAllItems(queryCmd: QueryCommand): Promise<any[]> {

        let result, exclusiveStartKey;
        let accumulatedResult: any[] = [];

        do {
            queryCmd.input.ExclusiveStartKey = exclusiveStartKey;
            // queryCmd.input.Limit = 100;
            result = await this.client.send(queryCmd);

            exclusiveStartKey = result.LastEvaluatedKey;
            accumulatedResult = accumulatedResult.concat(result.Items)
        } while(result.Items!.length || result.LastEvaluatedKey);

        return accumulatedResult;
    }
   async getItem(getItemCmd: GetItemCommand): Promise<GetItemCommandOutput> {
        return this.client.send(getItemCmd);
    }
    async putItem(putItemCmd: PutItemCommand): Promise<PutItemCommandOutput> {
        return this.client.send(putItemCmd);
    }
    async updateItem(updateItemCmd: UpdateItemCommand): Promise<UpdateItemCommandOutput> {
        return this.client.send(updateItemCmd);
    }
    async query(queryCmd: QueryCommand): Promise<QueryCommandOutput> {
        return this.client.send(queryCmd);
    }
    async scan(scanCmd: ScanCommand): Promise<ScanCommandOutput> {
        return this.client.send(scanCmd);
    }
    async deleteItem(deleteItemCmd: DeleteItemCommand): Promise<DeleteItemCommandOutput> {
        return this.client.send(deleteItemCmd);
    }

}