import { defaultAwsParams } from '../helpers';

import { AWSClientFactory } from '../clients/SingleAWSClientFactory'

import { GetSecretValueCommand, GetSecretValueCommandInput, GetSecretValueCommandOutput, SecretsManagerClient } from '@aws-sdk/client-secrets-manager';

import { SMClient } from '../clients/Aws.clients';

import { IAWSProvider } from './IAWSProvider';

export class AWSProvider implements IAWSProvider {
    awsClientFactory: AWSClientFactory; 
    smClient: SecretsManagerClient;

    constructor(){
        this.awsClientFactory = new AWSClientFactory();
        this.smClient = this.awsClientFactory.create(SecretsManagerClient);
    }

    async getSecretValue(cmdInput: GetSecretValueCommandInput): Promise<GetSecretValueCommandOutput> {

        try {

            const command: GetSecretValueCommand = new GetSecretValueCommand(cmdInput);
            const cmdOutput: GetSecretValueCommandOutput = await this.smClient.send(command);

            return cmdOutput;
        } catch (err) {
            console.error(err);
            throw err;
        }
    }
}