import { GetSecretValueCommandInput, GetSecretValueCommandOutput } from '@aws-sdk/client-secrets-manager';

export interface IAWSProvider {
    getSecretValue(params: GetSecretValueCommandInput): Promise<GetSecretValueCommandOutput>
}