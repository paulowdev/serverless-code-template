/*
 * Just common configuration variables.
 * 
 * DO NOT PUT SENSITIVE DATA HERE
 */

export const sharedConfig = {
    LOG_LEVEL : "INFO"
};