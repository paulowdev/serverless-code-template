import "reflect-metadata"
import { container } from 'tsyringe';

import { AWSProvider } from "./infrastructure/providers/AWSProvider";
import { SecretsManagerProvider } from "./infrastructure/providers/aws/SecretsManagerProvider";
import { SNSProvider } from "./infrastructure/providers/aws/SNSProvider";


container.registerSingleton("IAWSProvider", AWSProvider);
container.registerSingleton("ISecretsManagerProvider", SecretsManagerProvider);
container.registerSingleton("ISNSProvider", SNSProvider);


export const sharedContainer = container;